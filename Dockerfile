FROM docker:27.0

COPY --from=docker/buildx-bin:0.16.0 /buildx /usr/libexec/docker/cli-plugins/docker-buildx

# Docker Multi-Architecture Builder

## Description
This project provides a custom Dockerfile to create a Docker container capable of building images for multiple architectures. It is specifically designed for use in GitLab CI/CD pipelines to facilitate multi-architecture building and deployment.

## Features
- **Docker Base**: Uses `docker:27.0` as the base image.
- **Buildx Integration**: Includes the `buildx` plugin for multi-architecture support.
- **GitLab CI/CD Compatible**: Ready to be integrated into GitLab CI/CD for automated builds.

## Prerequisites
- Docker installed on your machine.
- GitLab account for CI/CD integration.

## Installation and Configuration
1. **Clone the Project**:
   ```
   git clone https://gitlab.com/maltyxx/docker/buildx
   ```
2. **Build the Docker Image**:
   ```
   docker build -t your_image_name .
   ```
3. **Usage in GitLab CI/CD**:
    - Incorporate the built image into your `.gitlab-ci.yml` file.
    - Use the image as the base for your Docker image building jobs.

## Usage
Ensure that your `.gitlab-ci.yml` file is set up to use this image for build stages. Example:
```yaml
build_image:
  stage: build
  image: your_image_name
  script:
    - docker buildx build --platform linux/amd64,linux/arm64 -t your_image_name .
```

## Contributing
Contributions to this project are welcome. Please follow these steps:
1. Fork the project.
2. Create your feature branch (`git checkout -b feature/AmazingFeature`).
3. Commit your changes (`git commit -m 'Add some AmazingFeature'`).
4. Push to the branch (`git push origin feature/AmazingFeature`).
5. Open a Pull Request.

## License
[MIT](LICENSE)
